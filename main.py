import pytube
import subprocess
from bs4 import BeautifulSoup
import time
import os
import requests
import re

playlist = [] #재생목록 저장용!
lists = [] #유투브 재생목록 내에 각각의 영상 링크들 저장용
url = input("Insert your youtube playlist url : ")
path = input("Insert your locale : ")
path += '/'

def listParshing(url): #각 영상의 링크들을 가져오기 위한 함수
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    countlist = soup.find_all("a", class_=" spf-link playlist-video clearfix yt-uix-sessionlink spf-link ") #영상 링크들 가져오기...
    for i in range(0, len(countlist), 1):
        strchange = str(countlist[i])
        cleanr = strchange[168:187]
        lists.append("http://www.youtube.com" + "/" + cleanr) #링크 추가!

def download(): #영상들을 받기 위한 함수
    for i in range(0, len(lists), 1):
        try:
            yt = pytube.YouTube(lists[i])
            playlist.append(yt.title) #순서를 맞추기 위한 함수
            if(os.path.exists(path + yt.title + ".mp3")): #이미 받아진 파일이면 pass
                continue
            yt.streams.filter(only_audio=True, subtype="mp4").first().download() #.mp4타입에 오디오만 있는 버전 다운
            subprocess.call(["ffmpeg", "-i", os.path.join(path + yt.title + ".mp4"),
            os.path.join(path + yt.title + ".mp3")]) #ffmpeg를 통하여, mp4 포맷을 mp3로 convert
            os.remove(path + yt.title + ".mp4") #기존 파일 제거

        except pytube.exceptions.RegexMatchError as e: # pytube 오류 핸들링...
            print(e)
            pass

if(__name__ == "__main__"):
    listParshing(url)
    download()
    playmode = len(playlist)
    nowplaying = 0
    while(1):
        if(nowplaying == playmode):
            nowplaying = 0
        print(playlist[nowplaying])
        subprocess.Popen(['mpg123', '-q', playlist[nowplaying] + ".mp3"]).wait()
        nowplaying+=1